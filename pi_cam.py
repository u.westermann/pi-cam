#/usr/bin/env python3

import datetime
import fractions
import numpy as np
import os
import picamera
import picamera.array
import smtplib
import ssl
import threading


MOTION_LEVEL_THRESHOLD = 60
MOTION_COUNT_THRESHOLD = 10
RECORDING_DURATION_SECONDS = 60
EMAIL_SMTP_SERVER = ''
EMAIL_FROM = ''
EMAIL_TO = ''
EMAIL_SMTP_PORT = 587
EMAIL_SMTP_USER = ''
EMAIL_SMTP_PASS = ''


motion_event = threading.Event()


def log(message):
    print(datetime.datetime.now().replace(microsecond=0).isoformat() + ': ' + message)


def send_mail():
    if EMAIL_SMTP_PASS:
        try:
            with smtplib.SMTP(EMAIL_SMTP_SERVER, EMAIL_SMTP_PORT) as smtp:
                smtp.starttls()
                smtp.login(EMAIL_SMTP_USER, EMAIL_SMTP_PASS)
                smtp.sendmail(EMAIL_FROM, [EMAIL_TO], f'From: CAMERA\nSubject: MOTION DETECTED\n\n{datetime.datetime.now().replace(microsecond=0).isoformat()}')
        except Exception as exc:
            log(f'error while sending email: {exc}')


class MotionDetector(picamera.array.PiMotionAnalysis):
    def analyze(self, a):
        a = np.sqrt(np.square(a['x'].astype(np.float)) + np.square(a['y'].astype(np.float))).clip(0, 255).astype(np.uint8)
        a_sum = (a > MOTION_LEVEL_THRESHOLD).sum()
        
        if a_sum > MOTION_COUNT_THRESHOLD:
            log(f'motion threshold exceeded: {a_sum}')
            motion_event.set()
            send_mail()


os.system('sudo bash -c "echo 0 > /sys/class/leds/led0/brightness"')

log(f'starting camera. MOTION_LEVEL_THRESHOLD={MOTION_LEVEL_THRESHOLD}, MOTION_COUNT_THRESHOLD={MOTION_COUNT_THRESHOLD}, RECORDING_DURATION_SECONDS={RECORDING_DURATION_SECONDS}.')

with picamera.PiCamera() as cam:
    with MotionDetector(cam) as mot_det:
        log('configuring camera')
        cam.sensor_mode = 4
        cam.iso = 800
        cam.framerate = fractions.Fraction(4, 1)
        cam.resolution = (820, 616)
        cam.vflip = True

        while True:
            log('waiting for motion...')
            cam.start_recording('/dev/null', format='h264', motion_output=mot_det)
            motion_event.wait()
            motion_event.clear()
            cam.stop_recording()

            log('start recording...')
            #cam.start_preview()
            cam.start_recording('./vids/' + datetime.datetime.now().replace(microsecond=0).isoformat() + '.h264', format='h264')
            cam.wait_recording(RECORDING_DURATION_SECONDS)
            log('stop recording')
            cam.stop_recording()
            #cam.stop_preview()
